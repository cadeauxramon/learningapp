package com.learningapp.mockserver.routes

import io.ktor.server.application.Application
import io.ktor.server.routing.Route
import io.ktor.server.routing.get
import io.ktor.server.routing.route
import io.ktor.server.routing.routing

fun Route.mockDataRouting(){
    route("/"){
        get {

        }
    }
}

fun Application.setPathToMock(){
    routing {
        mockDataRouting()
    }
}