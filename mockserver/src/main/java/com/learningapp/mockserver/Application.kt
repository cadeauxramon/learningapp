package com.learningapp.mockserver

import com.learningapp.mockserver.routes.setPathToMock
import io.ktor.serialization.kotlinx.json.json
import io.ktor.server.application.install
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.plugins.contentnegotiation.ContentNegotiation

fun main(){
    embeddedServer(Netty, port = 8016){
        install(ContentNegotiation){
            json()
        }

        setPathToMock()
    }.start(true)
}