package com.example.learningapp.di

import com.example.learningapp.network.datasource.PokemonOverviewDataSource
import com.example.learningapp.network.mapper.FetchListMapper
import com.example.learningapp.network.repository.PokemonOverviewRepository
import com.example.learningapp.network.usecase.FetchPokemonOverviewUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object PokemonOverviewModule {

    @Provides
    fun providePokemonOverviewRepository(pokemonOverviewDataSource: PokemonOverviewDataSource,
                                          fetchListMapper: FetchListMapper
    ): PokemonOverviewRepository = PokemonOverviewRepository(pokemonOverviewDataSource, fetchListMapper)

    @Provides
    fun providePokemonOverviewUseCase(pokemonOverviewRepository: PokemonOverviewRepository): FetchPokemonOverviewUseCase =
        FetchPokemonOverviewUseCase(pokemonOverviewRepository)
}