package com.example.learningapp.di

import com.example.learningapp.network.PokemonEndpoint
import com.example.learningapp.network.service.PokemonService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object Singletons {

    @Provides

    fun provideGson(): Gson {
        return Gson()
    }


    @Provides
    fun provideConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    fun providePokemonService(factory: GsonConverterFactory, endpoint: PokemonEndpoint): PokemonService {
        return Retrofit.Builder()
            .addConverterFactory(factory)
            .baseUrl(endpoint.endpoint)
            .build()
            .create(PokemonService::class.java)
    }

}