package com.example.learningapp.di

import com.example.learningapp.network.PokemonEndpoint
import com.example.learningapp.network.datasource.FetchListDataSourcePaging3
import com.example.learningapp.network.mapper.FetchListMapper
import com.example.learningapp.network.repository.FetchListRepositoryWithPaging3
import com.example.learningapp.network.service.PokemonService
import com.example.learningapp.network.usecase.FetchListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(ViewModelComponent::class)
object FetchListWithPagingModule {
    @Provides
    fun provideFetchRepository(
        pokemonListDataSource:FetchListDataSourcePaging3,
        pokemonFetchListMapper:FetchListMapper
    ):FetchListRepositoryWithPaging3= FetchListRepositoryWithPaging3(pokemonListDataSource,pokemonFetchListMapper)

    @Provides
    fun provideFetchListUseCase(
        fetchListRepository: FetchListRepositoryWithPaging3,
    ): FetchListUseCase = FetchListUseCase(fetchListRepository)

}