package com.example.learningapp.di

import com.example.learningapp.network.datasource.FetchPokemonDataSource
import com.example.learningapp.network.mapper.FetchPokemonUiMapper
import com.example.learningapp.network.repository.FetchPokemonRepository
import com.example.learningapp.network.usecase.FetchPokemonUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object PokemonModule {

    @Provides
    fun provideFetchPokemonRepository(
        fetchPokemonDataSource: FetchPokemonDataSource
    ):FetchPokemonRepository = FetchPokemonRepository(fetchPokemonDataSource)

    @Provides
    fun provideFetchPokemonUseCase(
        fetchPokemonRepository: FetchPokemonRepository,
        fetchPokemonUiMapper: FetchPokemonUiMapper
    ): FetchPokemonUseCase = FetchPokemonUseCase(fetchPokemonRepository,fetchPokemonUiMapper)
}