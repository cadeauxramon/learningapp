package com.example.learningapp.di

import com.example.learningapp.network.datasource.FetchPokemonDataSource
import com.example.learningapp.network.repository.DetailsRepository
import com.example.learningapp.network.usecase.DetailsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
object PokemonDetailsModule {

    @Provides
    fun provideDetailsUseCase(
        detailsRepository: DetailsRepository
    ) = DetailsUseCase(detailsRepository)

    @Provides
    fun provideDetailsRepository(
        fetchPokemonDataSource: FetchPokemonDataSource
    ) = DetailsRepository(fetchPokemonDataSource)
}