package com.example.learningapp.overview

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import coil.compose.AsyncImagePainter
import coil.request.ImageRequest
import com.example.learningapp.ErrorMessage
import com.example.learningapp.PageLoader
import com.example.learningapp.PokemonDestinations
import com.example.learningapp.R
import com.example.learningapp.listwithpaging3.PokemonListUiState
import com.example.learningapp.preparePathToDetailScreen
import com.example.learningapp.ui.theme.LearningAppTheme
import com.example.learningapp.ui.theme.extractLightColor

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PokemonOverviewScreen(
    viewmodel: PokemonOverviewViewModel = hiltViewModel(),
    navController: NavController = rememberNavController()
) {
    Surface(
        color = MaterialTheme.colorScheme.background,
        modifier = Modifier
            .fillMaxSize()
            .combinedClickable(onLongClick = {
                navController.navigate(PokemonDestinations.PokemonListScreen.destinationPath)
            }, onClick = {}, onDoubleClick = {
                viewmodel.paginatePokemonList()
            })
    ) {

        when (viewmodel.overviewState.collectAsState().value) {
            is PokemonListUiState.Loading -> PageLoader(Modifier.fillMaxSize())
            is PokemonListUiState.Error -> ErrorMessage(message = "Sorry Dog") {
                viewmodel.retryInit()
            }

            else -> {

                PokemonView(viewmodel, navController)
            }
        }

    }
}

@Composable
fun PokemonView(viewmodel: PokemonOverviewViewModel, navController: NavController) {
    val pokemonList: List<PokemonOverViewUiModel> = viewmodel.listState.collectAsState().value
    LazyVerticalGrid(columns = GridCells.Fixed(3), contentPadding = PaddingValues(10.dp)) {
        item(span = {
            GridItemSpan(3)
        }) {
            Column {
                Spacer(
                    modifier = Modifier
                        .height(25.dp)
                )
                Image(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(CenterHorizontally)
                        .size(200.dp),
                    painter = painterResource(id = R.drawable.noun_pokemon_3329414),
                    contentDescription = "App Logo"
                )

                Text(
                    text = "Using Pokemon as a pretext to Learn Compose and Clean Architecture",
                    fontSize = 12.sp
                )
            }
        }
        items(pokemonList) {
            if (pokemonList.indexOf(it) >= pokemonList.size - 3) viewmodel.paginatePokemonList()
            PokeDexEntry(it) {
                navController.navigate(preparePathToDetailScreen(it.index))
            }
        }
    }
}

@Composable
fun PokeDexEntry(
    pokemonOverViewUiModel: PokemonOverViewUiModel,
    onClick: () -> Unit
) {
    val defaultDominantColor = MaterialTheme.colorScheme.surface
    var domColor by remember {
        mutableStateOf(defaultDominantColor)
    }
    var lightColor by remember {
        mutableStateOf(defaultDominantColor)
    }

    Card(modifier = Modifier
        .fillMaxWidth()
        .fillMaxHeight()
        .background(
            Brush.verticalGradient(
                listOf(lightColor, domColor)
            )
        )
        .clickable {
            onClick()
        }) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    Brush.verticalGradient(
                        listOf(lightColor, domColor)
                    )
                )
                .align(CenterHorizontally)
        ) {
            Row(
                Modifier
                    .align(CenterHorizontally)

            ) {
                Text(text = pokemonOverViewUiModel.index.toString(),
                    fontWeight = FontWeight.Black,)
                AsyncImage(
                    modifier = Modifier
                        .size(120.dp),
                    onState = {
                        (it as? AsyncImagePainter.State.Success)?.let { image ->
                            image.result.drawable.extractLightColor().let { colors ->
                                lightColor = colors.first
                                domColor = colors.second
                            }

                        }
                    },
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(pokemonOverViewUiModel.imageUrl)
                        .placeholder(R.drawable.noun_pokemon_gym_727778)
                        .crossfade(true).build(),
                    contentDescription = pokemonOverViewUiModel.name
                )
            }
            Text(
                text = pokemonOverViewUiModel.name,
                fontWeight = FontWeight.Black,
                modifier = Modifier.align(CenterHorizontally),
                fontSize = 20.sp
            )
        }
    }
}


@Preview
@Composable
fun PreviewPokemonOverviewScreen(
) {
    LearningAppTheme {
        PokeDexEntry(PokemonOverViewUiModel(1, "Stupid Plant", "")) {}
    }
}


