package com.example.learningapp.overview

data class PokemonOverViewUiModel(val index:Int,val name: String, val imageUrl:String)
