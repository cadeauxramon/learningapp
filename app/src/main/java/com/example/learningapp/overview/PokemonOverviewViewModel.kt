package com.example.learningapp.overview

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.learningapp.listwithpaging3.PokemonListUiState
import com.example.learningapp.network.repository.OverviewDataRequest
import com.example.learningapp.network.usecase.FetchPokemonOverviewUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokemonOverviewViewModel @Inject constructor(
    private val fetchPokemonOverviewUseCase: FetchPokemonOverviewUseCase
):ViewModel(){

    private val _overviewState = MutableStateFlow<PokemonListUiState>(PokemonListUiState.Loading)
    val overviewState: StateFlow<PokemonListUiState> = _overviewState

    private val _listState= MutableStateFlow<List<PokemonOverViewUiModel>>(mutableListOf())
    val listState: StateFlow<List<PokemonOverViewUiModel>> = _listState

    init {
        viewModelScope.launch {
            fetchPokemonList(OverviewDataRequest.Initial).let { response ->
                _overviewState.update {
                    response
                }

                (response as? PokemonListUiState.PokemonList)?.let { newShit->
                    _listState.update {
                        val stuff= it.toMutableList()
                        stuff.addAll(newShit.listOfMons)
                        stuff
                    }

                }
            }


        }
    }


    private suspend fun fetchPokemonList(request: OverviewDataRequest): PokemonListUiState {
        return try {
            fetchPokemonOverviewUseCase.fetchData(request)
        } catch (e:Exception){
            Log.e("Oops","something not right",e)
            PokemonListUiState.Error
        }
    }

    fun paginatePokemonList(){
        viewModelScope.launch {
                (fetchPokemonList(OverviewDataRequest.Next) as? PokemonListUiState.PokemonList)?.let { newShit->
                    _listState.update {
                        val stuff= it.toMutableList()
                        stuff.addAll(newShit.listOfMons)
                        stuff
                }

            }

        }
    }

    fun retryInit() {
        TODO("Not yet implemented")
    }
}