package com.example.learningapp.ui.theme

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.compose.ui.graphics.Color
import androidx.palette.graphics.Palette

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

fun Drawable.extractLightColor(): Pair<Color, Color> {
    return (this as BitmapDrawable).bitmap.copy(Bitmap.Config.ARGB_8888, true).extractColor().let {
            Pair(it.lightMutedSwatch?.rgb?.let {lightColor->
                Color(lightColor)
            } ?: Color.White,
                it.lightVibrantSwatch?.rgb?.let {
                    domColor-> Color(domColor) } ?: Color.White)
        }
}
fun Bitmap.extractColor(): Palette =  Palette.from(this).generate()
