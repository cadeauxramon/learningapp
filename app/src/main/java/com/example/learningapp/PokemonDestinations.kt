package com.example.learningapp

enum class PokemonDestinations(val destinationPath:String) {
    PokemonListScreen("pokemonListScreen"),
    PokemonDetailsScreen("pokemonDetailsScreen/{pokemonIndex}"),
    PokemonSearchScreen("pokemonSearchScreen"),
    PokemonOverviewScreen("pokemonOverviewScreen")
}

fun preparePathToDetailScreen(pokemon:Int):String{
    return PokemonDestinations.PokemonDetailsScreen.destinationPath.replace("{pokemonIndex}",pokemon.toString())
}