package com.example.learningapp.details

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.learningapp.ErrorMessage
import com.example.learningapp.network.models.pokemon.DetailsUiModel
import com.example.learningapp.preparePathToDetailScreen

@Composable
fun PokemonDetailScreen(navController: NavHostController= rememberNavController(),
                        pokemonIndex: Int?, pokeDetailsViewModel: PokeDetailsViewModel = hiltViewModel()
){
    pokeDetailsViewModel.getPokemonDetails(pokemonIndex)

    when(val pokemonDetails = pokeDetailsViewModel.exposed.collectAsState().value){
        is DetailsUiModel.NotFound-> PokemonNotFound()
        is DetailsUiModel.Error -> ErrorMessage(message = "Encountered an Issue") {
            pokeDetailsViewModel.getPokemonDetails(pokemonIndex)
        }
        else-> {
            FullPokedexEntry(pokemonDetails as DetailsUiModel.Pokemon){
                navController.navigate(preparePathToDetailScreen(it))
            }
        }
    }
}

@Composable
fun FullPokedexEntry(pokemon: DetailsUiModel.Pokemon, onClickEvolution: (Int) -> Unit) {
    val defaultDominantColor = MaterialTheme.colorScheme.surface
    var domColor by remember {
        mutableStateOf(defaultDominantColor)
    }
    var lightColor by remember {
        mutableStateOf(defaultDominantColor)
    }

    Surface() {

    }

}

@Composable
fun PokemonNotFound() {

}
@Preview
@Composable
fun PokemonDetailsPreview() {
        FullPokedexEntry(pokemon = DetailsUiModel.Pokemon()) {
            
        }
}


