package com.example.learningapp.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.learningapp.network.models.pokemon.DetailsUiModel
import com.example.learningapp.network.usecase.DetailsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokeDetailsViewModel @Inject constructor(
    private val detailsUseCase: DetailsUseCase
):ViewModel() {
        private val backing= MutableStateFlow<DetailsUiModel>(DetailsUiModel.Loading)
     val exposed :StateFlow<DetailsUiModel> = backing


    fun getPokemonDetails(pokemonIndex:Int?){

        viewModelScope.launch {
            backing.update {
                pokemonIndex?.let {
                    if (pokemonIndex > 0) {
                        try {
                            detailsUseCase.fetchData(it)
                        } catch (e:Exception){
                            DetailsUiModel.Error
                        }
                    } else
                        DetailsUiModel.NotFound
                } ?: DetailsUiModel.NotFound
            }
        }
    }
}