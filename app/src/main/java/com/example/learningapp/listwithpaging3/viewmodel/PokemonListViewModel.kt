package com.example.learningapp.listwithpaging3.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.example.learningapp.network.usecase.FetchListUseCase
import com.example.learningapp.overview.PokemonOverViewUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PokemonListViewModel @Inject constructor(
    private val fetchList: FetchListUseCase
) : ViewModel() {

    private val uiStateMutable = MutableStateFlow<PagingData<PokemonOverViewUiModel>>(PagingData.empty())

    val uiState: StateFlow<PagingData<PokemonOverViewUiModel>> = uiStateMutable

    init {
        viewModelScope.launch {
           fetchList.fetchData(Unit)
               .catch {
                   uiStateMutable.update {
                       PagingData.empty()
                   }
               }
               .onEach{ newValue->
                   uiStateMutable.update {
                       newValue
                   }
               }
           }
        }




}