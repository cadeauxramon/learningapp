package com.example.learningapp.listwithpaging3

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemKey
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.learningapp.ErrorMessage
import com.example.learningapp.LoadingNextPageItem
import com.example.learningapp.PageLoader
import com.example.learningapp.PokemonDestinations
import com.example.learningapp.R
import com.example.learningapp.listwithpaging3.viewmodel.PokemonListViewModel
import com.example.learningapp.overview.PokemonOverViewUiModel
import com.example.learningapp.preparePathToDetailScreen
import com.example.learningapp.ui.theme.LearningAppTheme


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PokemonListScreen(
    modifier: Modifier,
    viewModel: PokemonListViewModel = hiltViewModel(),
    navController: NavController = rememberNavController()
) {
    Box(modifier = modifier.combinedClickable(onLongClick = {
        navController.navigate(PokemonDestinations.PokemonOverviewScreen.destinationPath)
    }, onClick = {})) {
        val value = viewModel.uiState.collectAsLazyPagingItems()
        PokemonSuccessState(value)
    }
}


@Preview
@Composable
fun PreviewListScreen() {
    LearningAppTheme {
        Surface {
            PokemonContent(
                data = PokemonOverViewUiModel(
                    1, "Bulbasuar",
                    "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
                )
            )
        }

    }
}

@Composable
fun PokemonListLoadingState(
    modifier: Modifier = Modifier,
    horizontalAlignment: Alignment.Horizontal = CenterHorizontally,
    verticalArrangement: Arrangement.HorizontalOrVertical = Arrangement.Center,
    color: Color = Color.Black
) {
    Column(
        modifier = modifier,
        horizontalAlignment = horizontalAlignment,
        verticalArrangement = verticalArrangement,
    ) {
        Text(
            modifier = Modifier
                .padding(8.dp),
            text = "Refresh Loading"
        )

        CircularProgressIndicator(color =color)
    }
}

@Composable
fun PokemonListErrorState() {
}

@Composable
fun PokemonSuccessState(success: LazyPagingItems<PokemonOverViewUiModel>) {
    LazyColumn {
        items(count = success.itemCount,
            key = success.itemKey { it },
            ) { index ->
            success[index]?.let {
                PokemonContent(data = it)
            }
        }
        when {
            success.loadState.refresh is LoadState.Loading -> {
                item {
                    PageLoader(modifier = Modifier.fillParentMaxSize())
                }
            }

            success.loadState.refresh is LoadState.Error -> {
                item {
                    ErrorMessage(
                        modifier = Modifier.fillParentMaxSize(),
                        message = "BROKEN",
                        onClickRetry = {
//                            retry()
                        })
                }
            }

            success.loadState.append is LoadState.Loading -> {
                item {
                     LoadingNextPageItem(modifier = Modifier)
                }
            }

            success.loadState.append is LoadState.Error -> {
                item {
                    ErrorMessage(
                        modifier = Modifier,
                        message = "Broken",
                        onClickRetry = {
//                            retry()
                        })
                }
            }

            else -> {}
        }


    }

}

@Composable
fun PokemonContent(
    data: PokemonOverViewUiModel,
    navController: NavController = rememberNavController()
) {

    Box(modifier = Modifier
        .fillMaxWidth()
        .wrapContentHeight()
        .clickable {
            navController.navigate(preparePathToDetailScreen(data.index))
        }) {
        Column(modifier = Modifier.fillMaxWidth()) {
            AsyncImage(
                modifier = Modifier
                    .size(120.dp)
                    .align(CenterHorizontally)
                    .fillMaxWidth(),
                model = ImageRequest.Builder(LocalContext.current)
                    .data(data.imageUrl).placeholder(R.drawable.ic_launcher_foreground)
                    .crossfade(true).build(),
                contentDescription = data.name
            )

            Text(text = data.name, Modifier.align(CenterHorizontally), fontSize = 20.sp)
        }
    }
}



