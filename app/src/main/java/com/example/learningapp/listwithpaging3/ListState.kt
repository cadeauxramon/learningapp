package com.example.learningapp.listwithpaging3

import com.example.learningapp.network.models.PokemonMetadata
import com.example.learningapp.overview.PokemonOverViewUiModel

sealed class PokemonListUiState{
    object Loading: PokemonListUiState()
    object Error : PokemonListUiState()

    object NoActionTaken :PokemonListUiState()
   data class PokemonList(val listOfMons: List<PokemonOverViewUiModel>, val nextList: String?,
                          val previousList:String?) :
        PokemonListUiState()
}

sealed class PokemonDataState{
    object Loading: PokemonDataState()
    object Error: PokemonDataState()

    data class Success(val data: PokemonMetadata): PokemonDataState()
}
