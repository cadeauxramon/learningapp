package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation7(

    @SerializedName("icons") val icons: Icons? = Icons(),
    @SerializedName("ultra-sun-ultra-moon")
    val ultraSunUltraMoon: UltraSunUltraMoon? = UltraSunUltraMoon()

)