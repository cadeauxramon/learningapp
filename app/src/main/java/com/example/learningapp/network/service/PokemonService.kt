package com.example.learningapp.network.service

import com.example.learningapp.network.models.PokemonMetadata
import com.example.learningapp.network.models.pokemon.DetailsUiModel
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url


interface PokemonService {

    @GET("/api/v2/pokemon/")
    suspend fun getInitialPokemonList(): PokemonMetadata

    @GET("/api/v2/pokemon/")
    suspend fun getPokemonList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): PokemonMetadata

    @GET
    suspend fun fetchNextListResult(@Url url: String): PokemonMetadata

    @GET("api/v2/pokemon/{id}")
    suspend fun fetchSpecificPokemonData(@Path("id") pokemonId: Int): DetailsUiModel.Pokemon

    @GET("api/v2/pokemon/{name}")
    suspend fun fetchSpecificPokemonData(@Path("name") pokemonId: String): DetailsUiModel.Pokemon
}