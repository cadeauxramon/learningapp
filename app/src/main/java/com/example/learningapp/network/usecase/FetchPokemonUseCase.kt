package com.example.learningapp.network.usecase

import com.example.learningapp.network.Mapper
import com.example.learningapp.network.UseCase
import com.example.learningapp.network.models.pokemon.DetailsUiModel.Pokemon
import com.example.learningapp.network.repository.FetchPokemonRepository
import com.example.learningapp.overview.PokemonOverViewUiModel

class FetchPokemonUseCase(private val pokemonRepository: FetchPokemonRepository,
                          private val fetchPokemonUiMapper: Mapper<Pokemon, PokemonOverViewUiModel>
): UseCase<Int, PokemonOverViewUiModel> {
    override suspend fun fetchData(model: Int): PokemonOverViewUiModel {
        return fetchPokemonUiMapper.map(pokemonRepository.fetchData(model))
    }
}