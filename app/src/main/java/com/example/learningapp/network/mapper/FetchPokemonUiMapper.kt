package com.example.learningapp.network.mapper

import com.example.learningapp.network.Mapper
import com.example.learningapp.network.models.pokemon.DetailsUiModel.Pokemon
import com.example.learningapp.overview.PokemonOverViewUiModel
import javax.inject.Inject

class FetchPokemonUiMapper @Inject constructor() :
    Mapper<Pokemon, PokemonOverViewUiModel> {
    override suspend fun map(model: Pokemon): PokemonOverViewUiModel {
        TODO("Not yet implemented")
    }
}