package com.example.learningapp.network.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.learningapp.listwithpaging3.PokemonListUiState
import com.example.learningapp.network.Datasource
import com.example.learningapp.network.Mapper
import com.example.learningapp.network.Repository
import com.example.learningapp.network.datasource.FetchListDataRequest
import com.example.learningapp.network.datasource.PokemonPagingSource
import com.example.learningapp.network.models.PokemonMetadata
import com.example.learningapp.overview.PokemonOverViewUiModel
import kotlinx.coroutines.flow.Flow

const val ITEMS_PER_PAGE= 20
class FetchListRepositoryWithPaging3(
    private val fetchListDataSource: Datasource<FetchListDataRequest, PokemonMetadata>,
    private val mapper: Mapper<PokemonMetadata, PokemonListUiState>
    ):Repository<Unit,
        Flow<PagingData<PokemonOverViewUiModel>>> {
    override suspend fun fetchData(model: Unit):
            Flow<PagingData<PokemonOverViewUiModel>> {
        return Pager(config = PagingConfig(ITEMS_PER_PAGE, prefetchDistance = 3, initialLoadSize = ITEMS_PER_PAGE),
            initialKey = null,
            pagingSourceFactory = {
                PokemonPagingSource(fetchListDataSource,mapper)
            }
        ).flow
    }
}