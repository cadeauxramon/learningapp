package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation3(

    @SerializedName("emerald") val emerald: Emerald? = Emerald(),
    @SerializedName("firered-leafgreen")
    val fireRedLeafGreen: FireRedLeafGreen? = FireRedLeafGreen(),
    @SerializedName("ruby-sapphire") val rubySapphire: RubySapphire? = RubySapphire()

)