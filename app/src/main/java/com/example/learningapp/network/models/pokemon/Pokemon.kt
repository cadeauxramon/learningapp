package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName

sealed class DetailsUiModel {

    data class Pokemon(
        @SerializedName("abilities")
        val abilities: ArrayList<Abilities> = arrayListOf(),
        @SerializedName("base_experience")
        val baseExperience: Int? = null,
        @SerializedName("forms")
        val forms: ArrayList<Forms> = arrayListOf(),
        @SerializedName("game_indices")
        val gameIndices: ArrayList<GameIndices> = arrayListOf(),
        @SerializedName("height")
        val height: Int? = null,
        @SerializedName("held_items")
        val heldItems: ArrayList<String> = arrayListOf(),
        @SerializedName("id")
        val id: Int? = null,
        @SerializedName("is_default")
        val isDefault: Boolean? = null,
        @SerializedName("location_area_encounters")
        val locationAreaEncounters: String? = null,
        @SerializedName("moves")
        val moves: ArrayList<Moves> = arrayListOf(),
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("order")
        val order: Int? = null,
        @SerializedName("past_types")
        val pastTypes: ArrayList<String> = arrayListOf(),
        @SerializedName("species")
        val species: Species? = Species(),
        @SerializedName("sprites")
        val sprites: Sprites? = Sprites(),
        @SerializedName("stats")
        val stats: ArrayList<Stats> = arrayListOf(),
        @SerializedName("types")
        val types: ArrayList<Types> = arrayListOf(),
        @SerializedName("weight")
        val weight: Int? = null
    ) : DetailsUiModel()

    data object Error : DetailsUiModel()
    data object NotFound : DetailsUiModel()
    data object Loading : DetailsUiModel()

}
