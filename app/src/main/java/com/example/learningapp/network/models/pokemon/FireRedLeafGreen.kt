package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class FireRedLeafGreen (

  @SerializedName("back_default"  ) val backDefault  : String? = null,
  @SerializedName("back_shiny"    ) val backShiny    : String? = null,
  @SerializedName("front_default" ) val frontDefault : String? = null,
  @SerializedName("front_shiny"   ) val frontShiny   : String? = null

)