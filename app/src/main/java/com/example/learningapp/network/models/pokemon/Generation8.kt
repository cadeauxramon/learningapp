package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation8 (
  @SerializedName("icons" ) val icons : Icons? = Icons()
)