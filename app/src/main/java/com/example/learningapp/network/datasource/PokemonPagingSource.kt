package com.example.learningapp.network.datasource

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.learningapp.listwithpaging3.PokemonListUiState
import com.example.learningapp.network.Datasource
import com.example.learningapp.network.Mapper
import com.example.learningapp.network.models.PokemonMetadata
import com.example.learningapp.overview.PokemonOverViewUiModel

class PokemonPagingSource(
    private val fetchListDataSource: Datasource<FetchListDataRequest, PokemonMetadata>,
    private val mapper: Mapper<PokemonMetadata, PokemonListUiState>
): PagingSource<Int, PokemonOverViewUiModel>() {
    override fun getRefreshKey(state: PagingState<Int, PokemonOverViewUiModel>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PokemonOverViewUiModel> {
        return try {
            Log.d(this.javaClass.name, "load: init")
            val callValue= params.key ?: 1
            val result = fetchListDataSource.fetchData(FetchListDataRequest(
                    callValue * 20
                )).run {
                    mapper.map(this) as? PokemonListUiState.PokemonList
                }
            Log.d(this.javaClass.name, "load: data retrieved")

            result?.let {

                LoadResult.Page(data = result.listOfMons ,prevKey = if (result.previousList.isNullOrEmpty()) null else callValue - 1,
                    nextKey = if (result.nextList.isNullOrEmpty()) null else callValue + 1)
            }
                ?: LoadResult.Error(Throwable("NO RESULTS"))

        } catch (e:Exception){
            Log.e(this.javaClass.name,"service failed",e)
            LoadResult.Error(e)
        }
    }
}