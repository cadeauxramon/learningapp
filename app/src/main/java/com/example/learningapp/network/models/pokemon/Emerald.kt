package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Emerald (

  @SerializedName("front_default" ) val frontDefault : String? = null,
  @SerializedName("front_shiny"   ) val frontShiny   : String? = null

)