package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation1 (

  @SerializedName("red-blue" ) val redBlue : RedBlue? = RedBlue(),
  @SerializedName("yellow"   ) val yellow   : Yellow?   = Yellow()

)