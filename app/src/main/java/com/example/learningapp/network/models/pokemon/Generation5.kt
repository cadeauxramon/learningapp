package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation5 (
  @SerializedName("black-white" ) val blackWhite : BlackWhite? = BlackWhite()
)