package com.example.learningapp.network.datasource

import com.example.learningapp.network.Datasource
import com.example.learningapp.network.models.pokemon.DetailsUiModel
import com.example.learningapp.network.service.PokemonService
import javax.inject.Inject

class FetchPokemonDataSource @Inject constructor(
    private val pokemonService: PokemonService
) : Datasource<Int, DetailsUiModel.Pokemon> {
    override suspend fun fetchData(model: Int): DetailsUiModel.Pokemon {
        return pokemonService.fetchSpecificPokemonData(model)
    }
}