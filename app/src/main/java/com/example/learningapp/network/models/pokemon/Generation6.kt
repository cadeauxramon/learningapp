package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation6(

    @SerializedName("omegaruby-alphasapphire")
    val omegaRubyAlphaSapphire: OmegaRubyAlphaSapphire? = OmegaRubyAlphaSapphire(),
    @SerializedName("x-y") val xY: XY? = XY()

)