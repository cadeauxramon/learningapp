package com.example.learningapp.network.usecase

import com.example.learningapp.network.Repository
import com.example.learningapp.network.UseCase
import com.example.learningapp.network.models.pokemon.DetailsUiModel

class DetailsUseCase(
    private val detailsRepository:Repository<Int, DetailsUiModel.Pokemon>
):UseCase<Int,Any> {
    override suspend fun fetchData(model: Int): DetailsUiModel.Pokemon {
        return detailsRepository.fetchData(model)
    }

}