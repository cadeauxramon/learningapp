package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Abilities (

  @SerializedName("ability"   ) val ability  : Ability? = Ability(),
  @SerializedName("is_hidden" ) val isHidden : Boolean? = null,
  @SerializedName("slot"      ) val slot     : Int?     = null

)