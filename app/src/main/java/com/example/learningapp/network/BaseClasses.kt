package com.example.learningapp.network

interface UseCase<T,U>{
    suspend fun fetchData(model:T):U
}
interface Mapper<T,U>{
    suspend fun map(model:T):U
}
interface Datasource<T,U>{
    suspend fun fetchData(model:T):U
}
interface Repository<T,U>{
    suspend fun fetchData(model:T):U
}
