package com.example.learningapp.network.repository

import com.example.learningapp.network.Datasource
import com.example.learningapp.network.Repository
import com.example.learningapp.network.models.pokemon.DetailsUiModel.Pokemon

class FetchPokemonRepository(
    private val fetchPokemonDataSource: Datasource<Int, Pokemon>
):Repository<Int,Pokemon>{
    override suspend fun fetchData(model: Int): Pokemon {
        return fetchPokemonDataSource.fetchData(model)
    }
}