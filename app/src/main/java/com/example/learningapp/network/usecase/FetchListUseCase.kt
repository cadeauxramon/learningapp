package com.example.learningapp.network.usecase

import androidx.paging.PagingData
import com.example.learningapp.network.Repository
import com.example.learningapp.network.UseCase
import com.example.learningapp.overview.PokemonOverViewUiModel
import kotlinx.coroutines.flow.Flow

class FetchListUseCase(
    private val fetchListRepository:Repository<Unit,
            Flow<PagingData<PokemonOverViewUiModel>>>,
):UseCase<Unit, Flow<PagingData<PokemonOverViewUiModel>>> {
    override suspend fun fetchData(model: Unit): Flow<PagingData<PokemonOverViewUiModel>> {
        return fetchListRepository.fetchData(model)
    }
}