package com.example.learningapp.network

import javax.inject.Inject

class PokemonEndpoint @Inject constructor() {

    val endpoint: String = "https://pokeapi.co/"
}