package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class GameIndices (

  @SerializedName("game_index" ) val gameIndex : Int?     = null,
  @SerializedName("version"    ) val version   : Version? = Version()

)