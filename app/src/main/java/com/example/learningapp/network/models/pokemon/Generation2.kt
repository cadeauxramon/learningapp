package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation2 (

  @SerializedName("crystal" ) val crystal : Crystal? = Crystal(),
  @SerializedName("gold"    ) val gold    : Gold?    = Gold(),
  @SerializedName("silver"  ) val silver  : Silver?  = Silver()

)