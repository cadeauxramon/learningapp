package com.example.learningapp.network.usecase

import com.example.learningapp.listwithpaging3.PokemonListUiState
import com.example.learningapp.network.UseCase
import com.example.learningapp.network.repository.OverviewDataRequest
import com.example.learningapp.network.repository.PokemonOverviewRepository

class FetchPokemonOverviewUseCase(
    private val pokemonOverviewRepository: PokemonOverviewRepository
):UseCase<OverviewDataRequest,Any> {
    override suspend fun fetchData(model: OverviewDataRequest): PokemonListUiState {
        return pokemonOverviewRepository.fetchData(model)
    }

}
