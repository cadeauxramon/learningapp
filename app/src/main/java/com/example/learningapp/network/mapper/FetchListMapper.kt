package com.example.learningapp.network.mapper

import androidx.compose.ui.text.capitalize
import com.example.learningapp.listwithpaging3.PokemonListUiState
import com.example.learningapp.network.Mapper
import com.example.learningapp.network.models.PokemonMetadata
import com.example.learningapp.network.models.Results
import com.example.learningapp.overview.PokemonOverViewUiModel
import javax.inject.Inject

class FetchListMapper @Inject constructor():Mapper<PokemonMetadata, PokemonListUiState> {
    override suspend fun map(model: PokemonMetadata): PokemonListUiState {
        return PokemonListUiState.PokemonList(
            model.results.compileListOfPokemon(),
            model.next,
            model.previous
        )
    }

    private fun ArrayList<Results>.compileListOfPokemon():List<PokemonOverViewUiModel>{
        return mutableListOf<PokemonOverViewUiModel>().apply {
            this@compileListOfPokemon.forEach {
                add(it.mapToUiModel())
            }
        }
    }
    private fun Results.mapToUiModel(): PokemonOverViewUiModel {
        val extractedIndex:Int = this.url.extractIndexFromUrl()
        return PokemonOverViewUiModel(
            index = extractedIndex,
            imageUrl = generateImageUrl(extractedIndex),
            name = this.name.sanitizeNames().replaceFirstChar { if (it.isLowerCase()) it.titlecase() else it.toString() }
        )
    }

    private fun String.extractIndexFromUrl():Int{
        return this.dropLastWhile { url->
            url.isDigit().not()
        }.run {
            this.takeLastWhile { it.isDigit() }
        }.toInt()
    }

    private fun generateImageUrl(extractedIndex: Int): String {
        return "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${extractedIndex}.png"
    }
}

private fun String.sanitizeNames(): String {
    return if (this.contains("-")){
        this.substring(0,indexOf("-"))
    } else this
}
