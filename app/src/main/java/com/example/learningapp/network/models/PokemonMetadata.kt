package com.example.learningapp.network.models

import com.google.gson.annotations.SerializedName


data class PokemonMetadata(
    @SerializedName("count") val count: Int? = null,
    @SerializedName("next") val next: String? = null,
    @SerializedName("previous") val previous: String? = null,
    @SerializedName("results") val results: ArrayList<Results> = arrayListOf()
)