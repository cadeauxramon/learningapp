package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Versions(

    @SerializedName("generation-i") val generation1: Generation1? = Generation1(),
    @SerializedName("generation-ii") val generation2: Generation2? = Generation2(),
    @SerializedName("generation-iii") val generation3: Generation3? = Generation3(),
    @SerializedName("generation-iv") val generation4: Generation4? = Generation4(),
    @SerializedName("generation-v") val generation5: Generation5? = Generation5(),
    @SerializedName("generation-vi") val generation6: Generation6? = Generation6(),
    @SerializedName("generation-vii") val generation7: Generation7? = Generation7(),
    @SerializedName("generation-viii") val generation8: Generation8? = Generation8()

)