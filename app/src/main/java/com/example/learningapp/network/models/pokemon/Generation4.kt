package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Generation4 (

  @SerializedName("diamond-pearl"        ) val diamondPearl        : DiamondPearl?        = DiamondPearl(),
  @SerializedName("heartgold-soulsilver" ) val heartGoldSoulSilver : HeartGoldSoulSilver? = HeartGoldSoulSilver(),
  @SerializedName("platinum"             ) val platinum             : Platinum?             = Platinum()

)