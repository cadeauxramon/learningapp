package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class VersionGroupDetails (

  @SerializedName("level_learned_at"  ) val levelLearnedAt  : Int?             = null,
  @SerializedName("move_learn_method" ) val moveLearnMethod : MoveLearnMethod? = MoveLearnMethod(),
  @SerializedName("version_group"     ) val versionGroup    : VersionGroup?    = VersionGroup()

)