package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Moves(

    @SerializedName("move") val move: Move? = Move(),
    @SerializedName("version_group_details")
    val versionGroupDetails: ArrayList<VersionGroupDetails> = arrayListOf()

)