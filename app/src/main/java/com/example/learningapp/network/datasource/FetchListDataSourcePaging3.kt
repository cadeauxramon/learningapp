package com.example.learningapp.network.datasource

import com.example.learningapp.network.Datasource
import com.example.learningapp.network.models.PokemonMetadata
import com.example.learningapp.network.service.PokemonService
import javax.inject.Inject

class FetchListDataSourcePaging3 @Inject constructor(
    private val pokemonService: PokemonService
) : Datasource<FetchListDataRequest, PokemonMetadata> {
    override suspend fun fetchData(model: FetchListDataRequest): PokemonMetadata {
        return pokemonService.getPokemonList(model.offset, model.limit)
    }
}

data class FetchListDataRequest(val offset:Int, val limit:Int= 20)