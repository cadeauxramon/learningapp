package com.example.learningapp.network.repository

import com.example.learningapp.listwithpaging3.PokemonListUiState
import com.example.learningapp.network.Repository
import com.example.learningapp.network.datasource.PokemonOverviewDataSource
import com.example.learningapp.network.mapper.FetchListMapper

class PokemonOverviewRepository(
    private val pokemonOverviewDataSource: PokemonOverviewDataSource,
    private val fetchListMapper: FetchListMapper
):Repository<OverviewDataRequest, PokemonListUiState> {

    var isEndReached = false
    var nextResultSet :String = ""
    val ensureOnlyUniqueCalls= mutableSetOf<String>()
    override suspend fun fetchData(model: OverviewDataRequest): PokemonListUiState {
        //This check ensure that we only fetch data from a given source 1 time and not in the case where we have reached the end of pagination
        return if (model == OverviewDataRequest.Initial || ensureOnlyUniqueCalls.add(nextResultSet) || isEndReached ) {
            val result = pokemonOverviewDataSource.fetchData(
                if (model == OverviewDataRequest.Initial)
                    null
                else nextResultSet

            ).run {
                fetchListMapper.map(this)
            }

            (result as? PokemonListUiState.PokemonList)?.let {
                nextResultSet = it.nextList ?: ""
                //we know we are at the end when the api says there is nothing ahead but plenty behind
                isEndReached = it.nextList.isNullOrEmpty() && !it.previousList.isNullOrEmpty()
            }

            result
        } else PokemonListUiState.NoActionTaken
    }
}

enum class OverviewDataRequest{
    Initial,Next
}