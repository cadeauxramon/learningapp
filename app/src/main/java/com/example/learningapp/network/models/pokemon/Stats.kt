package com.example.learningapp.network.models.pokemon

import com.google.gson.annotations.SerializedName


data class Stats(
    @SerializedName("base_stat") val baseStat: Int? = null,
    @SerializedName("effort") val effort: Int? = null,
    @SerializedName("stat") val stat: Stat? = Stat()

)