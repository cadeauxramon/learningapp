package com.example.learningapp.network.datasource

import com.example.learningapp.network.Datasource
import com.example.learningapp.network.models.PokemonMetadata
import com.example.learningapp.network.service.PokemonService
import javax.inject.Inject

class PokemonOverviewDataSource @Inject constructor(
    private val pokemonService: PokemonService
): Datasource<String?, PokemonMetadata> {
    override suspend fun fetchData(model: String?): PokemonMetadata {
        return model?.let {
            pokemonService.fetchNextListResult(it)
        } ?: pokemonService.getInitialPokemonList()
    }
}