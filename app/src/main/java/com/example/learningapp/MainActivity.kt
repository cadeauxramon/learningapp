package com.example.learningapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.learningapp.details.PokemonDetailScreen
import com.example.learningapp.listwithpaging3.PokemonListScreen
import com.example.learningapp.overview.PokemonOverviewScreen
import com.example.learningapp.ui.theme.LearningAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LearningAppTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PokemonNavGraph()
                }
            }
        }
    }
}

@Composable
fun PokemonNavGraph() {
    val navController = rememberNavController()
    NavHost(
        navController = navController ,
        startDestination = PokemonDestinations.PokemonOverviewScreen.name
    ) {
        composable(PokemonDestinations.PokemonListScreen.name) {
            PokemonListScreen(modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),navController = navController)
        }

        composable(PokemonDestinations.PokemonOverviewScreen.name){
            PokemonOverviewScreen(navController = navController)
        }
        composable(
            PokemonDestinations.PokemonDetailsScreen.destinationPath, arguments =
            listOf(navArgument("pokemonIndex") {
                type = NavType.IntType
            })
        ) {
            PokemonDetailScreen(pokemonIndex = it.arguments?.getInt("pokemonIndex"),navController = navController)
        }

//                    composable(PokemonDestinations.PokemonSearchScreen.name) {
//
//                    }
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    LearningAppTheme {
        PokemonNavGraph()
    }
}