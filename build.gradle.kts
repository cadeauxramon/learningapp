// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application") version "8.3.0" apply false
    id("org.jetbrains.kotlin.android") version "1.9.22" apply false
    id("org.jetbrains.kotlin.jvm") version "1.8.10" apply false
    id("io.ktor.plugin") version "2.3.2" apply false
    id("com.google.dagger.hilt.android") version "2.51" apply false
    id("com.diffplug.spotless") version "6.20.0"
    id("com.google.devtools.ksp") version "1.9.22-1.0.17" apply false
}